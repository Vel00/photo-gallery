from django.template import Library

# Создаём объект библиотеки тэгов
register = Library()


# Регистрируем функцию в качестве простого тэга
@register.simple_tag(takes_context=True)
def replace_parameter(context, name, value):
    """
    Заменить содержимое параметра запроса в адресе текущей страницы
    :param context: контекст отображения страницы
    :param name: название заменяемого параметра
    :param value: новое значение параметра
    :return: адрес новой страницы
    """

    # Получаем набор параметров запроса
    parameters = context['request'].GET.copy()
    # Заменяем выбранный параметр
    parameters[name] = value

    # Возвращаем адрес страницы
    return parameters.urlencode()

