from django.db import models

# Create your models here.
class Image(models.Model):
    """Изображение, загруженное пользователем"""
    name = models.CharField(max_length=50)  # Название изображения
    description = models.CharField(max_length=200)  # Описание изображения
    date = models.DateTimeField(auto_now_add=True)  # Дата загрузки изображения
    image = models.ImageField(upload_to='images/')   # Файл изображения




