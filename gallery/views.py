from django.shortcuts import render, redirect
from .models import Image
from .functions import get_page_for_query


def index(request):
    """Главная страница сайта"""

    # Получаем параметры запроса
    query = request.GET.get('query', '')    # Поисковый запрос
    page = int(request.GET.get('p', '1'))   # Номер страницы

    # Получаем объект страницы, содержащий изображения по заданному запросу
    page_obj = get_page_for_query(query, page)

    # Возвращаем ответ в виде html страницы
    return render(request, 'index.html', {
        'page_obj': page_obj, 'search_query': query})


def upload(request):
    """Страница загрузки изображений"""

    # Определение типа запроса
    if request.method == 'POST':
        # Создание нового изображения
        new_image = Image(
            name=request.POST.get('name'),
            description=request.POST.get('description'),
            image=request.FILES['image']
        )
        # Сохранение объекта в базе данных
        new_image.save()
        # Перенаправление на главную страницу
        return redirect('index')
    else:
        # Отображение страницы загрузки
        return render(request, 'upload.html')
