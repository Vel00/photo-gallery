from django.test import TestCase, override_settings
from django.core.files.uploadedfile import SimpleUploadedFile
from django.core.paginator import EmptyPage
from gallery.models import Image
from gallery.functions import get_page_for_query
from django.urls import reverse
from pathlib import Path
import shutil


test_dir = Path('test_data').resolve()
media_dir = test_dir / 'media'


def make_upload_data(num):
    return {
        'name': f'image {num}',
        'description': 'my image description',
        'image': SimpleUploadedFile(f'image {num}.png', b'file_content', content_type='image/png')
    }


class IndexTestCase(TestCase):
    @override_settings(MEDIA_ROOT=media_dir)
    def setUp(self):
        for i in range(15):
            Image.objects.create(**make_upload_data(i))

    def tearDown(self):
        Image.objects.all().delete()
        try:
            shutil.rmtree(test_dir)
        except OSError:
            pass

    def test_accessibility(self):
        response = self.client.get(reverse('index'))

        self.assertEqual(response.status_code, 200)

    def test_pagination(self):
        page = get_page_for_query('', 2)
        self.assertTrue(page.has_previous())
        self.assertTrue(page.has_next())
        self.assertEqual(page.previous_page_number(), 1)
        self.assertEqual(page.next_page_number(), 3)

        try:
            page = get_page_for_query('game dev', 1)
        except EmptyPage:
            self.fail('EmptyPage should not be raised')
        else:
            self.assertEqual(len(page), 0)


class UploadTestCase(TestCase):
    def tearDown(self):
        Image.objects.all().delete()
        try:
            shutil.rmtree(test_dir)
        except OSError:
            pass


    def test_accessibility(self):
        response = self.client.get(reverse('upload'))

        self.assertEqual(response.status_code, 200)

    @override_settings(MEDIA_ROOT=media_dir)
    def test_single_upload(self):
        image_file = SimpleUploadedFile('image.png', b'file_content', content_type='image/png')
        name = 'my image'
        description = 'my first image'

        self.client.post(reverse('upload'), {
            'name': name, 'description': description, 'image': image_file})

        image_object = Image.objects.get(name=name)
        self.assertEqual(image_object.description, description)
        self.assertEqual(image_object.image.name, 'images/image.png')

    @override_settings(MEDIA_ROOT=media_dir)
    def test_multiple_upload(self):
        for i in range(10):
            self.client.post(reverse('upload'), make_upload_data(i))

        self.assertEqual(Image.objects.count(), 10)
