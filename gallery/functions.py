from .models import Image
from django.core.paginator import Paginator


def get_page_for_query(query, page_number):
    """
    Возвращает страницу с изображениями по заданному запросу
    :param query: текст запроса
    :param page_number: номер страницы
    :return: объект страницы пагинатора
    """

    # Получаем изображения в названии или описании которых содержатся запрос
    # И сортируем по убыванию даты загрузки
    images = Image.objects.filter(name__contains=query).union(
        Image.objects.filter(description__contains=query)
    ).order_by('-date')

    # Создаем объект пагинатора, который помогает с распределением изображений по страницам
    p = Paginator(images, 5)

    # Возвращаем страницу пагинатора с заданным номером
    return p.get_page(page_number)
