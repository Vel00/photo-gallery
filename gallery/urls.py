from django.urls import path
from . import views

# Список страниц сайта
urlpatterns = [
    path('', views.index, name="index"),
    path('upload', views.upload, name="upload")
]